package rmap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RMaPWebApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RMaPWebApiApplication.class, args);
	}

}
